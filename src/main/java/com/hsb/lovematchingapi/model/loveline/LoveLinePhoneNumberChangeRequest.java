package com.hsb.lovematchingapi.model.loveline;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLinePhoneNumberChangeRequest {
    private String lovePhoneNumber;
}
