package com.hsb.lovematchingapi.model.loveline;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineItem {
    private Long memberId;
    private String memberName;
    private String memberPhoneNumber;
    private String lovePhoneNumber;
}
