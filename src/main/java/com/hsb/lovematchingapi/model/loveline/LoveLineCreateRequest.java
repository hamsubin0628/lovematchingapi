package com.hsb.lovematchingapi.model.loveline;

import com.hsb.lovematchingapi.entity.Member;
import jakarta.persistence.JoinColumn;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoveLineCreateRequest {
    private String  lovePhoneNumber;
}
