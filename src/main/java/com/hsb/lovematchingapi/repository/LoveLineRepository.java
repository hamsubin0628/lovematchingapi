package com.hsb.lovematchingapi.repository;

import com.hsb.lovematchingapi.entity.LoveLine;
import org.springframework.data.jpa.repository.JpaRepository;

public interface LoveLineRepository extends JpaRepository<LoveLine, Long> {
}
