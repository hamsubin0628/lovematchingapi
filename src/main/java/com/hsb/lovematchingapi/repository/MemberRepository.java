package com.hsb.lovematchingapi.repository;

import com.hsb.lovematchingapi.entity.Member;
import org.hibernate.boot.archive.internal.JarProtocolArchiveDescriptor;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MemberRepository extends JpaRepository<Member, Long> {
}
