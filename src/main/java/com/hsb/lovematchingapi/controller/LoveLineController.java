package com.hsb.lovematchingapi.controller;

import com.hsb.lovematchingapi.entity.Member;
import com.hsb.lovematchingapi.model.loveline.LoveLineCreateRequest;
import com.hsb.lovematchingapi.model.loveline.LoveLineItem;
import com.hsb.lovematchingapi.model.loveline.LoveLinePhoneNumberChangeRequest;
import com.hsb.lovematchingapi.model.loveline.LoveLineResponse;
import com.hsb.lovematchingapi.service.LoveLineService;
import com.hsb.lovematchingapi.service.MemberService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/love-line")
public class LoveLineController {
    private final MemberService memberService;
    private final LoveLineService loveLineService;

    @PostMapping("/new/member-id/{memberId}")
    public String setLoveLine(@PathVariable long memberId, @RequestBody LoveLineCreateRequest request){
        Member member = memberService.getData(memberId);
        loveLineService.setLoveLine(member, request);

        return "OK";
    }

    @GetMapping("/all")
    public List<LoveLineItem> getLoveLines(){
        return loveLineService.getLoveLines();
    }

    @GetMapping("/detail/{id}")
    public LoveLineResponse getLoveLine(@PathVariable long id){
        return loveLineService.getLoveLine(id);
    }

    @PutMapping("/phone-number/love-line-id/{loveLineId}")
    public String putPhoneNumber(@PathVariable long loveLineId, @RequestBody LoveLinePhoneNumberChangeRequest request){
        loveLineService.putPhoneNumber(loveLineId, request);

        return "OK";
    }
}
